terraform {
  backend "http" {
  }
}

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.47.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
  access_key = "<YOUR-AWS-ACCESS-KEY>"
  secret_key = "<YOUR-AWS-SECRET-ACCESS-KEY>"
}
