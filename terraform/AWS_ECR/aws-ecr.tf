resource "aws_ecr_repository" "ecr" {
  name                 = "flask-app-repo"
  image_tag_mutability = "MUTABLE"
}
