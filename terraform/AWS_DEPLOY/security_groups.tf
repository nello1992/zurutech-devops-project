/******************** SECURITY GROUPS *****************/

//ALB SG
resource "aws_security_group" "alb-sg" {
  name   = "zuru-alb-sg"
  vpc_id = aws_vpc.main_vpc.id

  //Inbound - Allows HTTPS traffic
  ingress {
    description = "Inbound"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  //Inbound - Allows HTTP traffic
  ingress {
    description = "Inbound"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.main_vpc.cidr_block]
  }

  //Outbound - Allows all the outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


//EC2 SG
resource "aws_security_group" "ec2-sg" {
  name   = "zuru-ec2-sg"
  vpc_id = aws_vpc.main_vpc.id

  //Inbound - Allows HTTP traffic
  ingress {
    description     = "Inbound"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb-sg.id]   //Allow only inbound traffic forwarded by ALB
  }

  //Inbound - Allows SSH
  ingress {
    description = "Inbound"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  //Outbound - Allows all the outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}