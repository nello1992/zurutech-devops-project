/************* ALB (Application Load Balancer)  &  ASG (Auto Scaling Group) ***********/

//Target Group
resource "aws_lb_target_group" "tg" {
  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }
  name        = "zuru-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.main_vpc.id
}


//ALB
resource "aws_lb" "alb" {
  name                             = "zuru-alb"
  internal                         = false
  load_balancer_type               = "application"
  subnets                          = ["${aws_subnet.sub1.id}", "${aws_subnet.sub2.id}", "${aws_subnet.sub3.id}"]
  enable_cross_zone_load_balancing = true
  security_groups                  = [aws_security_group.alb-sg.id]
}


//Create and attach a listener to the ALB
resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
}


//Launch Configuration for ASG
resource "aws_launch_configuration" "asg-lc" {
  name          = "zuru-asg-lc"
  image_id      = var.aws_ami
  instance_type = var.aws_instance_type
  security_groups      = ["${aws_security_group.ec2-sg.id}"]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name  //Assign IAM Role to EC2 instances
  associate_public_ip_address = true
  
  /* User data:
     - install & configure Docker
     - authenticate against ECR using docker exploiting IAM Role
     - Pull and Run docker image from ECR Repo
  */
  user_data = <<EOF
                    #!/bin/bash
                    sudo yum update -y
                    sudo amazon-linux-extras install docker
                    sudo service docker start
                    sudo usermod -a -G docker ec2-user

                    aws configure set region ${var.aws_region}
                    aws ecr get-login-password | docker login --username AWS --password-stdin ${var.aws_ecr}

                    docker pull ${var.aws_ecr}/${var.aws_ecr_repo}
                    docker run -p 80:8600 $(docker images | grep ${var.aws_ecr}/${var.aws_ecr_repo} | awk {'print $3'})
  EOF

  lifecycle {
    create_before_destroy = true
  }
}


//ASG
resource "aws_autoscaling_group" "asg" {
  name                 = "zuru-asg"
  launch_configuration = aws_launch_configuration.asg-lc.name
  min_size             = 3
  max_size             = 6
  vpc_zone_identifier  = ["${aws_subnet.sub1.id}","${aws_subnet.sub2.id}", "${aws_subnet.sub3.id}"]
  health_check_type    = "ELB"
  target_group_arns    = [aws_lb_target_group.tg.arn]  //Attach the target group used by ALB

  tag {
    key                 = "Name"
    value               = "zuru-asg"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
