/************ IAM ROLE for EC2s against ECR ****************/

//Define IAM role for EC2 against ECR
resource "aws_iam_role" "ec2_ecr_access_role" {
  name               = "ecr-role"
  assume_role_policy = file("${var.CONF_FILES_PATH}/assume_role_policy.json")
}

//Define IAM Policy to be attached to the IAM role
resource "aws_iam_policy" "ec2_ecr_policy" {
  name        = "ecr-policy"
  description = "ECR Policy"
  policy      = file("${var.CONF_FILES_PATH}/ec2_to_ecr_policy.json")
}

//Attach the IAM Policy to the IAM role
resource "aws_iam_policy_attachment" "ec2_ecr_role_attachment" {
  name       = "Attach ECR role to EC2"
  roles      = ["${aws_iam_role.ec2_ecr_access_role.name}"]
  policy_arn = aws_iam_policy.ec2_ecr_policy.arn
}

//Define an IAM Instance Profile using IAM role to be attaced to EC2 istances
resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2_profile"
  role = aws_iam_role.ec2_ecr_access_role.name
}