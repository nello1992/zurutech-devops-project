/********* NETWORK RESOURCES ************/

//VPC
resource "aws_vpc" "main_vpc" {
  cidr_block = var.vpc_cidr_block
  
  tags = {
    Name = "zuru-main-vpc"
  }
}


//INTERNET GATEWAY
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = "zuru-igw"
  }
}


//SUBNET 1
resource "aws_subnet" "sub1" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = var.sub_cidr_block[0]
  availability_zone = var.aws_azs[0]

  tags = {
    Name = "zuru-sub-1"
  }
}


//SUBNET 2
resource "aws_subnet" "sub2" {
  vpc_id             = aws_vpc.main_vpc.id
  cidr_block         = var.sub_cidr_block[1]
  availability_zone  = var.aws_azs[1]

  tags = {
    Name = "zuru-sub-2"
  }
}


//SUBNET 3
resource "aws_subnet" "sub3" {
  vpc_id             = aws_vpc.main_vpc.id
  cidr_block         = var.sub_cidr_block[2]
  availability_zone  = var.aws_azs[2]

  tags = {
    Name = "zuru-sub-3"
  }
}
