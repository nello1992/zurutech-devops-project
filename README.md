# ZuruTech DevOps Assignment
This repository refers to a ZuruTech Assignment.

Please refer to the "ZuruTech DevOps Assignment" pdf file in the root directory of this repository to get detailed information about this repo. 


## Test and Deploy
In order to perform all tests, please fill the following
- AWS_ACCESS_KEY and AWS_SECRET_ACCESS_KEY in:
    - .terraform/AWS_ECR/providers
    - .terraform/AWS_DEPLOY/terraform.tfvars
    - GitLab environment variables
- AWS_ACCOUNT_ID in ECR_REPO variable in:
    - .terraform/AWS_ECR/terraform.tfvars
    - .terraform/AWS_DEPLOY/terraform.tfvars
    - .gitlab-ci.yml

